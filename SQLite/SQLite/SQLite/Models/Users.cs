﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;

namespace SQLite.Models
{
    public class Users
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string Email { get; set; }
    }
}
