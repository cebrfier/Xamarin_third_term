﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WeatherApp
{
    public static class Constants
    {
        public static string OpenWeatherMapEndpoint = "https://api.openweathermap.org/data/2.5/weather";
        public static string OpenWeatherMapForecastEndpoint = "http://api.openweathermap.org/data/2.5/forecast";
        // TODO add wether for 5 days
        public static string OpenWeatherMapAPIKey = "0f64b9c16ae31eb70d747050229e76f4";
    }
}
